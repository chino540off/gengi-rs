use crate::common;

#[derive(PartialEq, Debug, Clone)]
pub enum State {
    On,
    Off,
}

impl From<u8> for State {
    fn from(v: u8) -> State {
        match v {
            0 => State::Off,
            _ => State::On,
        }
    }
}

impl From<State> for u8 {
    fn from(s: State) -> u8 {
        match s {
            State::Off => 0,
            State::On => 1,
        }
    }
}

impl From<bool> for State {
    fn from(v: bool) -> State {
        match v {
            false => State::Off,
            true => State::On,
        }
    }
}

impl std::fmt::Display for State {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            State::On => write!(f, "On"),
            State::Off => write!(f, "Off"),
        }
    }
}

impl State {
    fn switch(&mut self) -> Self {
        match self {
            Self::Off => {
                *self = State::On;
                self.clone()
            }
            Self::On => {
                *self = State::Off;
                self.clone()
            }
        }
    }

    fn set(&mut self, v: Self) -> Self {
        *self = v;
        self.clone()
    }
}

#[derive(PartialEq, Debug, Clone, serde::Serialize, serde::Deserialize)]
pub enum Command {
    Pulse(u64),
    Switch,
    On,
    Off,
}

#[derive(PartialEq, Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Action {
    pub command: Command,
}

pub trait Relay: Send + Sync {
    fn switch(&mut self) -> common::Result<State>;
    fn pulse(&mut self, dur_ms: u64) -> common::Result<State>;
    fn set(&mut self, state: State) -> common::Result<State>;

    fn kind(&self) -> &'static str;
    fn state(&self) -> common::Result<State>;
}

impl std::fmt::Display for dyn Relay {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "Relay {} state {}",
            self.kind(),
            match self.state() {
                Ok(state) => format!("{}", state),
                Err(e) => format!("Error {}", e),
            }
        )
    }
}

use serde::ser::SerializeStruct;

impl serde::ser::Serialize for dyn Relay {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::ser::Serializer,
    {
        // 3 is the number of fields in the struct.
        let mut s = serializer.serialize_struct("Relay", 2)?;

        s.serialize_field(
            "state",
            &match self.state() {
                Ok(state) => format!("{}", state),
                Err(_) => format!("degraded"),
            },
        )?;
        s.serialize_field("kind", self.kind())?;
        s.end()
    }
}

struct Stub {
    state: State,
}

impl Relay for Stub {
    fn switch(&mut self) -> common::Result<State> {
        Ok(self.state.switch())
    }

    fn pulse(&mut self, dur_ms: u64) -> common::Result<State> {
        self.set(State::On)?;
        std::thread::sleep(std::time::Duration::from_millis(dur_ms));
        self.set(State::Off)?;

        Ok(self.state.clone())
    }

    fn set(&mut self, state: State) -> common::Result<State> {
        Ok(self.state.set(state))
    }

    fn kind(&self) -> &'static str {
        "stub"
    }

    fn state(&self) -> common::Result<State> {
        Ok(self.state.clone())
    }
}

struct Composite {
    state: State,
    relays: Vec<Box<dyn Relay>>,
}

impl Relay for Composite {
    fn switch(&mut self) -> common::Result<State> {
        for relay in &mut self.relays {
            relay.switch()?;
        }

        Ok(self.state.switch())
    }

    fn pulse(&mut self, dur_ms: u64) -> common::Result<State> {
        for relay in &mut self.relays {
            relay.pulse(dur_ms)?;
        }

        Ok(self.state.clone())
    }

    fn set(&mut self, state: State) -> common::Result<State> {
        for relay in &mut self.relays {
            relay.set(state.clone())?;
        }

        Ok(self.state.set(state))
    }

    fn kind(&self) -> &'static str {
        "composite"
    }

    fn state(&self) -> common::Result<State> {
        Ok(self.state.clone())
    }
}

impl Relay for sysfs_gpio::Pin {
    fn switch(&mut self) -> common::Result<State> {
        match self.state() {
            Ok(State::Off) => Ok(self.set(State::On)?),
            Ok(State::On) => Ok(self.set(State::Off)?),
            Err(e) => Err(e),
        }
    }

    fn pulse(&mut self, dur_ms: u64) -> common::Result<State> {
        self.set(State::On)?;
        std::thread::sleep(std::time::Duration::from_millis(dur_ms));
        self.set(State::Off)?;

        Ok(self.state()?)
    }

    fn set(&mut self, state: State) -> common::Result<State> {
        self.set_value(State::into(state))?;
        Ok(self.state()?)
    }

    fn kind(&self) -> &'static str {
        "gpio"
    }

    fn state(&self) -> common::Result<State> {
        let value = self.get_value()?;

        Ok(State::from(value))
    }
}

pub fn stub() -> Box<dyn Relay> {
    Box::new(Stub { state: State::Off })
}

pub fn composite(relays: Vec<Box<dyn Relay>>) -> Box<dyn Relay> {
    Box::new(Composite {
        state: State::Off,
        relays: relays,
    })
}

pub fn gpio(n: u64) -> common::Result<Box<dyn Relay>> {
    let pin = sysfs_gpio::Pin::new(n);

    pin.export()?;
    pin.set_direction(sysfs_gpio::Direction::Out)?;
    pin.set_active_low(true)?;
    pin.set_value(0)?;

    Ok(Box::new(pin))
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::json;

    struct Error {}

    impl Relay for Error {
        fn switch(&mut self) -> common::Result<State> {
            Err(Box::new(sysfs_gpio::Error::Unsupported(format!("error"))))
        }

        fn pulse(&mut self, _dur_ms: u64) -> common::Result<State> {
            Err(Box::new(sysfs_gpio::Error::Unsupported(format!("error"))))
        }

        fn set(&mut self, _state: State) -> common::Result<State> {
            Err(Box::new(sysfs_gpio::Error::Unsupported(format!("error"))))
        }

        fn kind(&self) -> &'static str {
            "error"
        }

        fn state(&self) -> common::Result<State> {
            Err(Box::new(sysfs_gpio::Error::Unsupported(format!("error"))))
        }
    }

    fn error() -> Box<dyn Relay> {
        Box::new(Error {})
    }

    #[test]
    fn test_stub() {
        let mut relay = stub();

        assert_eq!(relay.state().unwrap(), State::Off);
        assert_eq!(relay.switch().unwrap(), State::On);
    }

    #[test]
    fn test_composite() {
        {
            let mut relay = composite(vec![stub(), stub(), stub()]);

            assert_eq!(relay.state().unwrap(), State::Off);
            assert_eq!(relay.switch().unwrap(), State::On);
        }
        {
            let mut s1 = stub();

            assert_eq!(s1.state().unwrap(), State::Off);
            assert_eq!(s1.switch().unwrap(), State::On);

            let mut relay = composite(vec![s1, stub()]);

            assert_eq!(relay.state().unwrap(), State::Off);
            assert_eq!(relay.switch().unwrap(), State::On);
            assert_eq!(relay.set(State::Off).unwrap(), State::Off);
        }
        {
            let mut relay = composite(vec![error(), stub(), stub()]);

            assert!(!relay.switch().is_ok());
            assert_eq!(relay.state().unwrap(), State::Off);
        }
    }

    #[test]
    fn test_json() {
        {
            println!("{}", json!(stub()));
            println!("{}", json!(composite(vec!(stub(), stub()))));
            println!("{}", json!(gpio(16)));
            println!("{}", json!(error()));
        }
        {
            assert_eq!(
                serde_json::from_str::<Action>("{\"command\": \"Off\"}").unwrap(),
                Action {
                    command: Command::Off,
                },
            );
            assert_eq!(
                serde_json::from_str::<Action>("{\"command\": \"On\"}").unwrap(),
                Action {
                    command: Command::On,
                },
            );
            assert_eq!(
                serde_json::from_str::<Action>("{\"command\": {\"Pulse\": 50}}").unwrap(),
                Action {
                    command: Command::Pulse(50),
                },
            );
            assert_eq!(
                serde_json::from_str::<Action>("{\"command\": \"Switch\"}").unwrap(),
                Action {
                    command: Command::Switch,
                },
            );
        }
    }
}
