#[macro_use]
extern crate log;

use clap::Parser;
use hyper::body::Buf;

mod common;
mod relay;
//
fn failure(
    status: hyper::StatusCode,
    body: String,
) -> common::Result<hyper::Response<hyper::Body>> {
    error!("-> {} {}", status, body);
    Ok(hyper::Response::builder()
        .status(status)
        .body(body.into())
        .unwrap())
}

fn ok(relay: &Box<dyn relay::Relay>) -> common::Result<hyper::Response<hyper::Body>> {
    Ok(hyper::Response::builder()
        .status(hyper::StatusCode::OK)
        .header(hyper::header::CONTENT_TYPE, "application/json")
        .header(hyper::header::ACCESS_CONTROL_ALLOW_ORIGIN, "*")
        .body(hyper::Body::from(serde_json::to_string(&relay)?))
        .unwrap())
}

fn command(
    action: relay::Action,
    relay: &mut Box<dyn relay::Relay>,
) -> common::Result<relay::State> {
    match action.command {
        relay::Command::Pulse(dur_ms) => relay.pulse(dur_ms),
        relay::Command::Switch => relay.switch(),
        relay::Command::On => relay.set(relay::State::On),
        relay::Command::Off => relay.set(relay::State::Off),
    }
}

async fn get_relay(
    relay: std::sync::Arc<std::sync::Mutex<Box<dyn relay::Relay>>>,
) -> common::Result<hyper::Response<hyper::Body>> {
    let relay = relay.lock().unwrap();
    ok(&relay)
}

async fn post_relay(
    req: hyper::Request<hyper::Body>,
    relay: std::sync::Arc<std::sync::Mutex<Box<dyn relay::Relay>>>,
) -> common::Result<hyper::Response<hyper::Body>> {
    let whole_body = hyper::body::aggregate(req).await?;
    let action: relay::Action = serde_json::from_reader(whole_body.reader())?;

    let mut relay = relay.lock().unwrap();

    match command(action, &mut relay) {
        Ok(state) => {
            info!("Relay change state to {}", state);
            ok(&relay)
        }
        Err(e) => {
            error!("Relay fails to change state: {}", e);
            failure(
                hyper::StatusCode::INTERNAL_SERVER_ERROR,
                "Internal server error".to_string(),
            )
        }
    }
}

async fn ping() -> common::Result<hyper::Response<hyper::Body>> {
    Ok(hyper::Response::builder()
        .status(hyper::StatusCode::OK)
        .body("".into())
        .unwrap())
}

async fn api(
    req: hyper::Request<hyper::Body>,
    relay: std::sync::Arc<std::sync::Mutex<Box<dyn relay::Relay>>>,
) -> common::Result<hyper::Response<hyper::Body>> {
    let method = req.method().clone();
    let path = req.uri().path().to_string();

    match (method, path) {
        (hyper::Method::GET, path) => match path.split("/").collect::<Vec<&str>>().as_slice() {
            ["", "api"] => get_relay(relay).await,
            ["", "ping"] => ping().await,
            _ => failure(hyper::StatusCode::NOT_FOUND, "Not found".to_string()),
        },
        (hyper::Method::POST, path) => match path.split("/").collect::<Vec<&str>>().as_slice() {
            ["", "api"] => post_relay(req, relay).await,
            [..] => failure(hyper::StatusCode::NOT_FOUND, "Not found".to_string()),
        },
        _ => failure(hyper::StatusCode::NOT_FOUND, "Not found".to_string()),
    }
}

#[derive(clap::Parser, Debug)]
#[clap(author, version, about)]
/// Relay API
struct Arguments {
    #[clap(default_value_t = std::net::IpAddr::V4(std::net::Ipv4Addr::new(0, 0, 0, 0)), short, long)]
    /// listen HTTP port
    listen_ip: std::net::IpAddr,

    #[clap(default_value_t = 80, short, long)]
    /// listen HTTP port
    port: u16,

    #[clap(subcommand)]
    /// type of relay
    relay: Relay,
}

#[derive(clap::Subcommand, Debug)]
enum Relay {
    /// Create a Stub relay
    Stub {},
    /// Create a GPIO relay
    GPIO {
        #[clap(forbid_empty_values = true, short, long)]
        /// Pin number of the GPIO
        pin: u64,
    },
}

#[tokio::main]
pub async fn main() -> common::Result<(), hyper::Error> {
    let args = Arguments::parse();
    let relay = std::sync::Arc::new(std::sync::Mutex::new(match args.relay {
        Relay::Stub {} => relay::stub(),
        Relay::GPIO { pin } => match relay::gpio(pin) {
            Ok(relay) => relay,
            Err(_) => panic!("Cannot initialize GPIO for pin {}", pin),
        },
    }));
    pretty_env_logger::init();
    let addr = std::net::SocketAddr::new(args.listen_ip, args.port);

    let make_service = hyper::service::make_service_fn(move |_| {
        let relay = relay.clone();

        async move {
            let relay = relay.clone();
            Ok::<_, hyper::Error>(hyper::service::service_fn(move |req| {
                info!("{} {}", req.method(), req.uri().path());

                let relay = relay.clone();

                api(req, relay)
            }))
        }
    });

    let server = hyper::Server::bind(&addr).serve(make_service);

    info!("Gengi is listening on http://{}...", addr);

    server.await
}
