use askama::Template;

#[derive(Default, Template)]
#[template(path = "index.html")]
pub struct Index {}
