FROM --platform=$TARGETPLATFORM alpine

ARG TARGETPLATFORM

COPY target/${TARGETPLATFORM}/gengi /gengi

ENTRYPOINT ["/gengi"]
